package ru.inno;

public class Main {

    public static String mergeDocuments(Iterable<String> documents) {
        StringBuilder mergedDocument = new StringBuilder();

        Iterator<String> documentsIterator = documents.iterator();

        while (documentsIterator.hasNext()) {
            mergedDocument.append(documentsIterator.next() + " ");
        }
        return mergedDocument.toString();
    }
    public static void main(String[] args) {
        ArrayList<String> stringList = new ArrayList<String>();

        stringList.add("Hello!");
        stringList.add("Bye!");
        stringList.add("Fine!");
        stringList.add("Hello!");
        stringList.add("C++!");
        stringList.add("PHP!");
        stringList.add("Cobol!");

        testRemoveAt(stringList);
        testRemoveNoExistElement(stringList);
        testRemove(stringList);

    }
    public static void testRemoveAt(ArrayList<String> stringList) {
        stringList.removeAt(4);
        String documents = mergeDocuments(stringList);
        assert documents.equals("Hello! Bye! Fine! Hello! PHP! Cobol! ");
    }
    public static void testRemoveNoExistElement(ArrayList<String> stringList) {
        stringList.remove("Good!");
        String documents = mergeDocuments(stringList);
        assert documents.equals("Hello! Bye! Fine! Hello! PHP! Cobol! ");
    }
    public static void testRemove(ArrayList<String> stringList) {
        stringList.remove("Hello!");
        String documents = mergeDocuments(stringList);
        assert documents.equals("Bye! Fine! Hello! PHP! Cobol! ");
    }
}