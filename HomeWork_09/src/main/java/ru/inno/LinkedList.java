package ru.inno;

public class LinkedList<T> implements List<T> {

    private Node<T> first;

    private Node<T> last;
    private int count;

    private static class Node<E> {
        E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }
    }
    @Override
    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        if (count == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }
        this.last = newNode;
        count++;
    }

    @Override
    public void remove(T element) {

    }

    @Override
    public boolean contains(T element) {
        Node<T> current = this.first;
        while (current != null) {
            if (current.value.equals(element)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {

    }

    @Override
    public T get(int index) {
        if (0 <= index && index <= count) {
            Node<T> current = this.first;

            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        return null;

    }

    private class LinkedListIterator implements Iterator<T> {

        private Node<T> current = first;

        @Override
        public T next() {
            T value = current.value;
            current = current.next;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }
}
