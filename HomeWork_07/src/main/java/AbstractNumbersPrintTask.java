public abstract class AbstractNumbersPrintTask implements Task {

    @Override
    public abstract void complete();

    int from;
    int to;

    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }
}