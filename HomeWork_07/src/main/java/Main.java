public class Main {
    public static void main(String[] args) {

        Main main = new Main();

        EvenNumbersPrintTask evenNumbersPrintTask = new EvenNumbersPrintTask(1, 7);
        EvenNumbersPrintTask evenNumbersPrintTask1 = new EvenNumbersPrintTask(3, 11);

        EvenNumbersPrintTask[] evenNumbersPrintTasksArray = {
                evenNumbersPrintTask,
                evenNumbersPrintTask1
        };

        OddNumbersPrintTask oddNumbersPrintTask = new OddNumbersPrintTask(1, 7);
        OddNumbersPrintTask oddNumbersPrintTask1 = new OddNumbersPrintTask(3, 11);

        OddNumbersPrintTask[] oddNumbersPrintTasksArray = {
                oddNumbersPrintTask,
                oddNumbersPrintTask1
        };

        main.completeAllTasks(evenNumbersPrintTasksArray);
        main.completeAllTasks(oddNumbersPrintTasksArray);


    }

    void completeAllTasks(Task[] tasks) {
        for (Task task: tasks) task.complete();
    }
}