import Utils.StringUtils;

import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String [] args){

        StringUtils stringUtils = new StringUtils();
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();

        HashMap<Integer, String> wordsMap = stringUtils.parseStringToMap(str);

        int count = 0;
        for(int tmp : wordsMap.keySet())
        {
            if(count < tmp) count = tmp;
        }

        System.out.println(wordsMap.get(count) + " " + count);
    }
}
