package Utils;

import java.util.HashMap;

public class StringUtils {

    public HashMap<Integer, String> parseStringToMap(String str){

        HashMap<Integer, String> wordsMap = new HashMap<>();

        String[] words = str.split(" ");

        for (String word : words) {
            wordsMap.put(this.countOccurrences(words, word) , word);
        }

        return wordsMap;
    }
    public int countOccurrences(String [] words, String word){

        int count = 0;
        for(String tmp : words){
            if(tmp.equals(word)) count++;
        }

        return count;
    }
}

