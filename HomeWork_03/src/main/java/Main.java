import java.util.Scanner;

public class Main {

    public static void main(String [] args)
    {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество элементов массива");
        int amount = scanner.nextInt();

        int [] arrayOfNumbers = new int[amount];

        System.out.println("Введите элементы массива");
        for(int i = 0; i < amount; i++)
        {
            arrayOfNumbers[i] = scanner.nextInt();
        }

        int count = 0;

        System.out.println("Количество локальных минимумов");
        for(int i = 0; i < amount; i++)
        {
            if(i == 0)
                if(arrayOfNumbers[i + 1] > arrayOfNumbers[i])
                    count++;

            if(i != 0 && i != amount - 1)
                if(arrayOfNumbers[i - 1] > arrayOfNumbers[i] && arrayOfNumbers[i + 1] > arrayOfNumbers[i])
                    count++;

            if(i == amount - 1)
                if(arrayOfNumbers[i - 1] > arrayOfNumbers[i])
                    count++;
        }

        System.out.println(count);

    }

}
