package repo;

import model.Product;
import java.io.*;
import java.util.List;

public class ProductRepositoryFileBasedImpl implements ProductRepository {
    private final String fileName;

    public ProductRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Product> findAll() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            return bufferedReader.lines()
                    .map(ProductRepositoryFileBasedImpl::parseProduct)
                    .toList();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Product findById(Integer id) {
        return findAll().stream()
                .filter(product -> product.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        return findAll().stream()
                .filter(product -> product.getName().toLowerCase().contains(title.toLowerCase()))
                .toList();
    }

    @Override
    public void update(Product product)  {
        List<Product> products = findAll();
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false))) {
            bufferedWriter.write("");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, true))) {

                for(int i = 0; i < products.size(); i++){
                        if (products.get(i).getId().equals(product.getId())) {
                            bufferedWriter.write(product.toLine());
                            bufferedWriter.newLine();
                        }
                            bufferedWriter.write(products.get(i).toLine());
                            bufferedWriter.newLine();
                        }
                    } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }


    private static Product parseProduct(String product) {
        String[] productFields = product.split("\\|");

        return new Product(Integer.valueOf(productFields[0]), productFields[1],
                Double.valueOf(productFields[2]), Integer.valueOf(productFields[3]));
    }
}
