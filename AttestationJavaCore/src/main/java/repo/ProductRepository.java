package repo;

import model.Product;

import java.io.IOException;
import java.util.List;

public interface ProductRepository {
    List<Product> findAll();

    Product findById(Integer id);

    List<Product> findAllByTitleLike(String title);

    void update(Product product);
}
