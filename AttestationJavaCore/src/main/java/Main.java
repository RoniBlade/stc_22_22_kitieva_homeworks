import model.Product;
import repo.ProductRepository;
import repo.ProductRepositoryFileBasedImpl;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args)  {
        ProductRepositoryFileBasedImpl productRepository = new ProductRepositoryFileBasedImpl("products.txt");

        List<Product> products = productRepository.findAll();

        assert productRepository.findById(16).toLine().equals("16|Лимон без кожуры|220.2|29");

        assert productRepository.findAllByTitleLike("АбР").get(0).toLine().equals("1|Абрикос|214.9|43");
        assert productRepository.findAllByTitleLike("Йва").get(1).toLine().equals("3|Айва|159.6|32");

        Product melon = productRepository.findById(11);
        melon.setPrice(120.0);
        melon.setCount(135);
        productRepository.update(melon);
        assert productRepository.findById(11).toLine().equals("11|Дыня|120.0|135");


    }
}

