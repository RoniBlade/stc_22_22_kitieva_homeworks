import java.io.IOException;

public class Main {

    public static void main(String [] args) throws IOException {
        ATM atm = new ATM(1000, 300, 1500);



        assert atm.putMoney(-100) == -1 : "Отрицательное число нельзя вводить!";
        assert atm.putMoney(100) == 0 : "Ошибка банкомата";
        assert atm.putMoney(300) == 0 : "Ошибка банкомата";
        assert atm.putMoney(3000) != 0: "Обратитесь в банк, я съел " + (3000 - atm.maxVolumeAmount) + "рублей";


        assert atm.giveMoney(-100) != -1: "Отрицательные числа нельзя вводить";
        assert atm.giveMoney(100) != 0: "Ошибка банкомата";
        assert atm.giveMoney(300) != 0: "Ошибка банкомата";
        assert atm.giveMoney(400) == 0: "Извините, но больше снять нельзя";
        assert atm.giveMoney(1000) == 0: "Извините, но в банкомате больше нет денег либо у вас на счету недостаточно средств";



    }
}
