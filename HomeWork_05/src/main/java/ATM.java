public class ATM {

    int remainingAmount;
    int maxAllowAmount;
    int maxVolumeAmount;
    int count = 0;


    int giveMoney(int needAmount){
        if (needAmount >= 0) {
            if (needAmount <= maxAllowAmount && needAmount <= remainingAmount && needAmount >= 0) {
                count++;
                remainingAmount = remainingAmount - needAmount;
                return needAmount;
            } else return 0;
        }
        else return -1;
    }

    public int putMoney(int needAmount){
        if(needAmount >=0) {
            count++;
            if (needAmount > maxVolumeAmount) {
                remainingAmount = remainingAmount + (needAmount - maxVolumeAmount);
                return needAmount - maxVolumeAmount;
            } else {
                remainingAmount = remainingAmount + needAmount;
                return 0;
            }
        }
        else return -1;
    }


    public ATM(int remainingAmount, int maxAllowAmount, int maxVolumeAmount) {
        this.remainingAmount = remainingAmount;
        this.maxAllowAmount = maxAllowAmount;
        this.maxVolumeAmount = maxVolumeAmount;
    }

    public int getremainingAmount() {
        return remainingAmount;
    }

    public void setremainingAmount(int amount) {
        this.remainingAmount = amount;
    }

    public int getMaxAllowAmount() {
        return maxAllowAmount;
    }

    public void setMaxAllowAmount(int maxAllowAmount) {
        this.maxAllowAmount = maxAllowAmount;
    }

    public int getMaxVolumeAmount() {
        return maxVolumeAmount;
    }

    public void setMaxVolumeAmount(int maxVolumeAmount) {
        this.maxVolumeAmount = maxVolumeAmount;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
