public class Main {

    public static void main(String [] args){


    ArraysTasksResolver arraysTasksResolver = new ArraysTasksResolver();

        ArrayTask task = (array, from, to) -> {
            int sum = 0;
            for(int i = from; i < to; i++){
                sum+= i;
            }
            return sum;
        };

        ArrayTask task1 = (array, from, to) -> {

            int max = -9999, sum = 0;

            for(int i = from - 1; i < to; i++){
                if(array[i] > max)
                    max = array[i];
            }

            while(max != 0){
                sum += (max % 10);
                max/=10;
            }
            return sum;
        };


        int[] array = new int[]{0, 1, 2, 356, 4, 5, 6};

        int from = 1;
        int to = 5;


        arraysTasksResolver.resolveTask(array, task, from, to);
        arraysTasksResolver.resolveTask(array, task1, from, to);


    }

}
