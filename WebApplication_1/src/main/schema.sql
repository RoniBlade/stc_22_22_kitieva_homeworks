drop table if exists course;

create table student
(
    id         bigserial primary key,
    email      varchar(20) unique,
    password   varchar(255),
    first_name varchar(20) default 'DEFAULT_FIRSTNAME',
    last_name  varchar(20) default 'DEFAULT_LASTNAME',
    age        integer check (age >= 0 and age <= 120) not null,
    is_worker  bool
);
alter table student
    alter column email set not null;
alter table student
    add column average double precision check ( average >= 0 and average <= 5) default 0;


update student
set first_name = 'Марсель',
    last_name  = 'Сидиков'
where id = 1;
update student
set first_name = 'Станислав',
    last_name  = 'Жербеков'
where id = 2;
update student
set first_name = 'Юрий',
    last_name  = 'Вертелецкий'
where id = 3;
update student
set first_name = 'Владислав',
    last_name  = 'Малышев'
where id = 4;

create table course
(
    id          serial primary key,
    title       char(10) not null,
    description char(500),
    start       timestamp,
    finish      timestamp
);

create table lesson
(
    id          serial primary key,
    name        char(20),
    summary     char(1000),
    start_time  time,
    finish_time time,
    course_id   integer not null,
    foreign key (course_id) references course (id)
);

create table student_course (
                                student_id bigint,
                                course_id bigint,
                                foreign key (student_id) references student(id),
                                foreign key (course_id) references course(id)
);

alter table lesson
    alter column course_id drop not null;
