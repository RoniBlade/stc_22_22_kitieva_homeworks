insert into student (email, password, age, is_worker)
values ('marsel@gmail.com', 'qwerty007', 28, true);
insert into student (email, password, age, is_worker)
values ('stanislav@mail.ru', 'qwerty008', 38, false);
insert into student (email, password, age, is_worker)
values ('yurii@yandex.ru', 'qwerty009', 27, true);
insert into student (email, password, age, is_worker)
values ('vladislav@yahoo.com', 'qwerty010', 20, false);

insert into course (title, description, start, finish)
values ('Java', 'Разработка на Java', '2022-11-07', '2023-02-02');
insert into course (title, description, start, finish)
values ('PHP', 'Разработка на PHP', '2022-10-01', '2024-02-02');
insert into course (title, description, start, finish)
values ('SQL', 'Введение в работу с БД', '2022-09-01', '2025-02-02');
insert into course (title, description, start, finish)
values ('Spring', 'Разработка на Java с Использованием Spring', '2020-01-01', '2022-01-01');

insert into lesson (name, summary, start_time, finish_time, course_id)
values ('ООП', 'Определение объекта и класса', '09:00', '12:00', 1),
       ('Java лучше PHP', 'Потому-что ...', '10:00', '15:00', 2),
       ('Индексы в БД', 'Есть btree', '10:00', '16:00', 3),
       ('Бины', 'Бины в Spring', '09:00', '15:00', 4);

insert into lesson (name, summary, start_time, finish_time, course_id)
values ('План запроса', 'Explain Analyze', '10:00', '17:00', 3);

insert into student_course (student_id, course_id)
values (1, 1),
       (1, 2),
       (2, 3),
       (4, 3),
       (1, 3),
       (2, 4)