package ru.inno.webapp.ec.service;

import ru.inno.webapp.ec.dto.LessonForm;
import ru.inno.webapp.ec.model.Lesson;
import ru.inno.webapp.ec.model.User;

import java.util.List;

public interface LessonsService {
    List<Lesson>getAllLessons();
    Lesson getLesson(Long lessonId);
    void deleteLesson(Long lessonId);
    void addLesson(LessonForm lesson);
    void updateLesson(Long lessonId,LessonForm lesson);

}
