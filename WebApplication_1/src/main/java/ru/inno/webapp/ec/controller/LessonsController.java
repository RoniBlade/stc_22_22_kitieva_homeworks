package ru.inno.webapp.ec.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.inno.webapp.ec.dto.LessonForm;
import ru.inno.webapp.ec.service.LessonsService;

@RequiredArgsConstructor
@RequestMapping(value = "/lessons")
@Controller
public class LessonsController {
    private final LessonsService lessonsService;
    @GetMapping
    public String getLessonsPage (Model model){
        model.addAttribute("lessons",lessonsService.getAllLessons());
        return "lessons_page";
    }
    @GetMapping ("/{lesson-id}")
    public String getLessonPage(@PathVariable("lesson-id")Long lessonId,Model model){
        model.addAttribute("lesson",lessonsService.getLesson(lessonId));
        return "lesson_page";
    }
    @PostMapping
    public String addLesson(LessonForm lesson) {
        lessonsService.addLesson(lesson);
        return "redirect:/lessons/";
    }
    @GetMapping("/{lesson-id}/delete")
    public String updateLesson(@PathVariable("lesson-id") Long lessonId) {
        lessonsService.deleteLesson(lessonId);
        return "redirect:/lessons/";
    }
    @PostMapping("/{lesson-id}/update")
    public String updateLesson(@PathVariable("lesson-id") Long lessonId, LessonForm lesson) {
        lessonsService.updateLesson(lessonId, lesson);
        return "redirect:/lessons/" + lessonId;
    }
}
