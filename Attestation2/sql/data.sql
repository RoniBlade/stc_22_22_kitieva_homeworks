insert into account (first_name, last_name, number, experience, age, is_have_license, rights_category, rating)
values ('Malika', 'Kitieva', '+89648542366', 10, 28, true, 'M', 5),
       ('Joe', 'Black', '+89288936539', 25, 60, true, 'M', 4),
       ('Ivan', 'Babarin', '+89624592365', 2, 22, true, 'B', 3),
       ('Ilya', 'Petuckhov', '+89507682394', 7, 25, true, 'M', 3),
       ('Semen', 'Saprin', '+89089283475', 10, 28, true, 'M', 5);

insert into car (model, color, car_number, owner_id)
values ('Mercedes Benz', 'black', 'Б777ОГ',1),
       ('KAWASAKI NINJA H2R', 'blue', '5643AE',2),
       ('BMW X6', 'green', 'А453СС', 3),
       ('Lexus RX 2022', 'black', 'Е724ОО',4),
       ('Tesla Model X', 'grey', 'А198СА',5);



insert into trip (driver_id, car_id, travel_data, trip_duration)
values (1, 1, '2022-11-07', 3),
       (2, 2, '2022-12-08', 5),
       (3, 3, '2022-10-15', 6),
       (4, 3, '2022-10-17', 3),
       (5, 4, '2022-06-07', 1)





