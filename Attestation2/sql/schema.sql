
drop table account;
drop table car;
drop table if exists trip;


create table account (
    id serial primary key,
    first_name char(20) not null,
    last_name char(20) not null,
    number char(12) unique not null,
    experience integer check (experience >= 0 and experience <= 256) not null,
    age integer check (age >= 18 and (experience - age) <= 18) not null,
    is_have_license bool not null,
    rights_category char(2) not null,
    rating integer check (rating >= 0 and rating <= 5) not null
);

create table car(
    id serial primary key,
    model char(20) not null,
    color char(20) not null,
    car_number char(6) not null unique,
    owner_id bigint,
    foreign key (owner_id) references account(id)
);


create table trip(
    driver_id bigint,
    car_id bigint,
    travel_data timestamp not null,
    trip_duration integer not null check (trip_duration >= 0),
    foreign key (driver_id) references account(id),
    foreign key (car_id) references car(id)
);