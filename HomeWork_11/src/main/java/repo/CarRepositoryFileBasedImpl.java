package repo;

import model.Car;
import java.io.*;
import java.util.List;


public class CarRepositoryFileBasedImpl implements CarRepository {
    private final String fileName;

    public CarRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private List<Car> getAll() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            return bufferedReader.lines()
                    .map(CarRepositoryFileBasedImpl::parseCar)
                    .toList();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Car> findByColor(String color) {
        return getAll().stream()
                .filter(car -> car.getColor().equalsIgnoreCase(color))
                .toList();
    }

    @Override
    public List<Car> findByMileage(int mileage) {
        return getAll().stream()
                .filter(car -> car.getColor().equals(mileage))
                .toList();
    }

    @Override
    public Integer countUniqueModelsInRange(int startPrice, int endPrice) {
        return Math.toIntExact(getAll().stream()
                .filter(car -> car.getPrice() > startPrice).
                filter(car -> car.getPrice() < endPrice ).count());
    }

    @Override
    public String getColorByMinPrice() {
        return getAll().stream().sorted(Car::compareTo).findFirst().get().getColor();
    }

    @Override
    public double getAveragePriceByModel(String model) {

        List<Car> cars = getAll().stream()
                .filter(car -> car.getModel().equalsIgnoreCase(model))
                .toList();
        int sum = 0;
        for(Car c : cars){
            sum += c.getPrice();
        }

        return sum/(double)cars.size();

    }
    private static Car parseCar(String car) {
        String[] carFields = car.split("\\|");

        return new Car(carFields[0], carFields[1],
                carFields[2], Integer.valueOf(carFields[3]), Integer.valueOf(carFields[4]));
    }
}
