package repo;

import model.Car;

import java.util.List;

public interface CarRepository {

    List<Car> findByColor(String color);
    List<Car> findByMileage(int mileage);
    Integer countUniqueModelsInRange(int startPrice, int endPrice);
    String getColorByMinPrice();
    double getAveragePriceByModel(String model);

}
