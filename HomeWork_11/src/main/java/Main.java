import model.Car;
import repo.CarRepositoryFileBasedImpl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args)  {

        CarRepositoryFileBasedImpl carRepository = new CarRepositoryFileBasedImpl("car.txt");

        List<String> rightAnswer1Task = new ArrayList<String>();
        rightAnswer1Task.add("o001aa111");
        rightAnswer1Task.add("o001aa111");

        List<String> rightAnswer2Task = new ArrayList<String>();
        rightAnswer2Task.add("o002aa111");

        List<Car> cars = carRepository.findByColor("BLACK");
        for(Car c : cars)
            assert rightAnswer1Task.contains(c.getNumber());

        cars = carRepository.findByMileage(0);
        for(Car c : cars)
            assert rightAnswer2Task.contains(c.getNumber());

        assert carRepository.countUniqueModelsInRange(700000, 800000).equals(0);

        assert carRepository.getColorByMinPrice().equals("Green");

        DecimalFormat df = new DecimalFormat("#.##");
        assert df.format(carRepository.getAveragePriceByModel("Camry")).equals("54666,67");

    }
}

