public class Figure {

    protected int x_coordinateOfTheCenter;
    protected int y_coordinateOfTheCenter;

    protected Figure(int x_coordinateOfTheCenter, int y_coordinateOfTheCenter) {
        this.x_coordinateOfTheCenter = x_coordinateOfTheCenter;
        this.y_coordinateOfTheCenter = y_coordinateOfTheCenter;
    }

    protected double getPerimeter(){return 0;}
    protected double getSquare(){return 0;}
    protected void moveTo(int toX, int toY){return 0;}

}
