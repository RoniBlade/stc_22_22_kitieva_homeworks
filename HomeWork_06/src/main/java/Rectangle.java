public class Rectangle extends Figure {

    protected int height;
    protected int width;


    protected Rectangle(int x_coordinateOfTheCenter, int y_coordinateOfTheCenter, int height, int width) {
        super(x_coordinateOfTheCenter, y_coordinateOfTheCenter);
        this.height = height;
        this.width = width;
    }


    @Override
    protected double getPerimeter(){
        return 2*(height + width);
    }

    @Override
    protected double getSquare(){
        return height*width;
    }

    @Override
    protected void moveTo(int toX, int toY){
        x_coordinateOfTheCenter = toX;
        y_coordinateOfTheCenter = toY;
    }

}
