public class Circle extends Ellipse{

    int radius;


    public Circle(int x_coordinateOfTheCenter, int y_coordinateOfTheCenter, int radius) {
        super(x_coordinateOfTheCenter, y_coordinateOfTheCenter, radius, radius);
        this.radius = radius;
}

    @Override
    protected double getPerimeter(){
        return 4*((3.14*radius*radius + Math.pow(radius + radius, 2))/(radius + radius));
    }

    @Override
    protected double getSquare(){
        return 3.14*radius*radius;
    }

    @Override
    protected void moveTo(int toX, int toY){
        x_coordinateOfTheCenter = toX;
        y_coordinateOfTheCenter = toY;
    }

}
