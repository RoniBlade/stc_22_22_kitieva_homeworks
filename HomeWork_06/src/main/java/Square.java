public class Square extends  Rectangle{

    protected int length;

    public Square(int x_coordinateOfTheCenter, int y_coordinateOfTheCenter,int length) {
        super(x_coordinateOfTheCenter, y_coordinateOfTheCenter, length, length);
        this.length = length;
    }


    @Override
    protected double getPerimeter(){
        return 2*(length + length);
    }

    @Override
    protected double getSquare(){
        return length*length;
    }

    @Override
    protected void moveTo(int toX, int toY){
        x_coordinateOfTheCenter = toX;
        y_coordinateOfTheCenter = toY;
    }



}
