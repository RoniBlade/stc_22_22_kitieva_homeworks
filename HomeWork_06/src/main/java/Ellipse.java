public class Ellipse extends  Figure{

    int bigRadius;
    int littleRadius;

    public Ellipse(int x_coordinateOfTheCenter, int y_coordinateOfTheCenter, int bigRadius, int littleRadius) {
        super(x_coordinateOfTheCenter, y_coordinateOfTheCenter);
        this.bigRadius = bigRadius;
        this.littleRadius = littleRadius;
    }

    @Override
    protected double getPerimeter(){
        return 4*((3.14*bigRadius*littleRadius + Math.pow(bigRadius + littleRadius, 2))/(bigRadius + littleRadius));
    }

    @Override
    protected double getSquare(){
        return 3.14*bigRadius*littleRadius;
    }

    @Override
    protected void moveTo(int toX, int toY){
        x_coordinateOfTheCenter = toX;
        y_coordinateOfTheCenter = toY;
    }


}
