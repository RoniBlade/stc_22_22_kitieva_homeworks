public class Main {

    public static void main(String[] args) {

        int sum = checkInterval(1,6);
        System.out.println("Сумма чисел: " + sum);

        int [] a = {12,1,2,3,4,5,6,7};
        printEvenNumbers(a);

        long ar = toInt(a);
        System.out.println("Число: " + ar);

    }

    public static int checkInterval(int a, int b){
        if(a > b){
            return -1;
        }
        else{

            int sum = 0;
            for(int i = a; i <= b; i++){
                sum += i;
            }

            return sum;

        }
    }


    public static void printEvenNumbers(int [] array){

        System.out.print("Четные числа: ");

        for(int i = 0; i < array.length; i++){

            if(array[i]%2 == 0) {
                System.out.print(array[i] + " ");
            }
        }
        System.out.println();
    }

    public static long toInt(int [] nums){

        long result = 0;
        long multiplier = 1;
        for (int i = nums.length; i > 0; --i) {
            int num = nums[i - 1];
            while (num != 0) {
                result += multiplier * (num % 10);
                num /= 10;
                multiplier *= 10;
            }
        }
        return result;

    }


}
